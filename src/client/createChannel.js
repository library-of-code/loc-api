const axios = require('axios');

/**
 * 
 * @param {string} userID Your user ID.
 * @param {string} clientID 
 * @param {any} time The time in minutes, can be either a string or a number. It's automatically converted in this function and on the API itself.
 * @param {string} authorization Your authorization token.
 * @async
 * @returns {Promise} Server doesn't really return further information.
 * @summary This endpoint allows you to create a temporary & private channel for you and your bot if you need to conduct testing. The time limit is 30 minutes with a minimum of 1 minute.
 * @example .client.createChannel('my ID', 'my bots ID', '5', 'my token').then(r => console.log(r));
 */

async function createChannel(userID, clientID, time, authorization) {
  if (typeof userID !== 'string') throw new TypeError('Parameter \'userID\' was expected to be a string');
  if (typeof clientID !== 'string') throw new TypeError('Parameter \'clientID\' was expected to be a string');
  if (typeof authorization !== 'string') throw new TypeError('Parameter \'authorization\' was expected to be a string');

  const parseTime = parseInt(time);
    
  const method = await axios({
    method: 'post',
    url: `https://api.libraryofcode.us/client/${clientID}/channels`,
    headers: {
      authorization: authorization,
      userID: userID,
      time: parseTime
    }
  });
  return method.data;
}

module.exports = createChannel;