const axios = require('axios');

/**
 * 
 * @param {string} ID The ID of the client application.
 * @async
 * @summary This function allows you to get information on a client application that has been approved in the server.
 * @returns {Promise} Returns an object with properties "client" and "approval".
 * @example .client.getClient('id').then(r => console.log(r)); 
 */

async function getClient(ID) {
  if (!ID) throw new TypeError('Parameter \'ID\' is required');
  const method = await axios({
    method: 'get',
    url: `https://api.libraryofcode.us/client/${ID}`,
    headers: {
      authorization: '446067825673633794'
    }
  });
  return method.data;
}

module.exports = getClient;
