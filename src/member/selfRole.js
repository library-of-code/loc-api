const axios = require('axios');

/**
 * 
 * @param {string} userID Your own user ID.
 * @param {string} roleID The ID of the role you want, the role has to be a self-assignable role.
 * @param {string} authorization Your authorization token.
 * @async
 * @summary This allows you to obtain a selfrole in the server, you can only obtain already self-assignable ranks and roles. There are two examples here, the bottom one is there if you don't want to record the response from the server, since the response is generally useless.
 * @returns {Promise} The response from the server, usually contains nothing.
 * @link https://www.libraryofcode.ml/docs/member Further information.
 * @example .member.selfRole('my id', 'role id', 'my token').then(r => console.log(r));
 * @example .member.selfRole('my id', 'role id', 'my token');
 */

async function selfRole(userID, roleID, authorization) {
  if (!userID) throw new TypeError('Parameter \'userID\' is required');
  if (!roleID) throw new TypeError('Parameter \'roleID\' is required');
  if (!authorization) throw new TypeError('Parameter \'authorization\' is required');
  const method = await axios({
    method: 'put',
    url: `https://api.libraryofcode.us/member/${userID}/roles/${roleID}`,
    headers: {
      authorization: authorization
    }
  });
  return method.data;
}

module.exports = selfRole;