const axios = require('axios');

/**
 * 
 * @param {string} userID Your user ID.
 * @param {string} roleID The role ID of the role you wish to remove.
 * @param {string} authorization Your authorization token.
 * @async
 * @summary This removes any role you wish to remove, literally any. They don't have to be a self role. If you remove a role on accident and can't give it to back to yourself, you'll need to contact a Management or Council member for help.
 * @returns {Promise} The response from the server, usually contains nothing useful.
 * @example .member.removeRole('my id', 'the role id', 'my token').then(r => console.log(r)); 
 */

async function removeRole(userID, roleID, authorization) {
  if (!userID) throw new TypeError('Parameter \'userID\' is required');
  if (!roleID) throw new TypeError('Parameter \'roleID\' is required');
  if (!authorization) throw new TypeError('Parameter \'authorization\' is required');
  const method = await axios({
    method: 'delete',
    url: `https://api.libraryofcode.us/member/${userID}/roles/${roleID}`,
    headers: {
      authorization: authorization
    }
  });
  return method.data;
}

module.exports = removeRole;