const axios = require('axios');

/**
 * 
 * @param {string} ID The user ID of the member to get.
 * @async
 * @summary Pulls up information on a member in the guild.
 * @returns {Promise} Returns an object containing properties "user" and "member".
 * @example .member.getMember('some id').then(r => console.log(r));
 */

async function getMember(ID) {
  if (!ID) throw new TypeError('Parameter \'ID\' is required');
  const method = await axios({
    method: 'get',
    url: `https://api.libraryofcode.us/member/${ID}`,
    headers: {
      authorization: '446067825673633794'
    }
  });
  return method.data;
}

module.exports = getMember;