module.exports = {
  client: {
    getClient: require('./src/client/getClient.js'),
    createChannel: require('./src/client/createChannel')
  },
  member: {
    getMember: require('./src/member/getMember'),
    removeRole: require('./src/member/removeRole'),
    selfRole: require('./src/member/selfRole')
  }
};